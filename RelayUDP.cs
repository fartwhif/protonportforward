﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace protonportforward
{
    internal class RelayUDP
    {
        public int listenPort;
        public string listenHost;
        public int targetPort;
        public string targetHost;

        public static IPEndPoint m_listenEp = null;
        public static IPEndPoint epRemote = null;
        public static IPEndPoint epApp = null;
        public static Socket socketRemote = null;
        public static Socket socketApp = null;

        public RelayConfig cfg = null;
        public ConcurrentDictionary<string, RelayUDP> pool = null;
        public CancellationTokenSource Exiter = new CancellationTokenSource();
        public ManualResetEvent DoneExiting = new ManualResetEvent(false);
        public ManualResetEvent DoneStarting = new ManualResetEvent(false);

        public RelayUDP(RelayConfig cfg, ConcurrentDictionary<string, RelayUDP> relays)
        {
            pool = relays;
            this.cfg = cfg;
            listenHost = cfg.listenHost;
            listenPort = cfg.listenPort;
            targetHost = cfg.targetHost;
            targetPort = cfg.targetPort;
        }

        public void Run()
        {
            pool[cfg.GetHash()] = this;
            TcpListener server = null;
            try
            {
                m_listenEp = new IPEndPoint(IPAddress.Any, listenPort);
                socketRemote = new Socket(m_listenEp.Address.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                socketRemote.Bind(m_listenEp);
                Console.WriteLine($"UDP listening on {m_listenEp}");

                epApp = new IPEndPoint(IPAddress.Parse(targetHost), targetPort);
                epRemote = new IPEndPoint(IPAddress.Any, targetPort);

                Queue<IEnumerable<byte>> ForRemote = new Queue<IEnumerable<byte>>();
                Queue<IEnumerable<byte>> ForApp = new Queue<IEnumerable<byte>>();

                socketApp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                int wait = 0;
                bool a, b;

                DoneStarting.Set();
                Stopwatch sw = Stopwatch.StartNew();
                while (true)
                {
                    if (Exiter.IsCancellationRequested)
                    {
                        Console.WriteLine($"Shutting down UDP proxy {listenHost}:{listenPort}");
                        break;
                    }
                    a = Read(socketRemote, ref epRemote, ForApp);
                    b = Write(socketApp, epApp, ForApp);
                    //c = Read(socketApp, ref epApp, ForRemote);
                    //d = Write(socketRemote, epRemote, ForRemote);
                    if (a || b)// || c || d)
                    {
                        wait = 0;
                        sw.Restart();
                    }
                    else
                    {
                        wait = Math.Min(1 + (wait * 2), 200);
                    }
                    if (wait > 0)
                    {
                        Thread.Sleep(wait);
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                server?.Stop();
                DoneStarting.Set();
            }
            pool[cfg.GetHash()] = null;
            DoneExiting.Set();
        }
        private bool Read(Socket readFrom, ref IPEndPoint readFrom2, Queue<IEnumerable<byte>> writeTo)
        {
            if (readFrom.Available < 1)
            {
                return false;
            }

            byte[] bytes = new byte[8192];
            List<byte> allBytes = new List<byte>();
            EndPoint x = new IPEndPoint(IPAddress.Any, targetPort);
            int i = readFrom.ReceiveFrom(bytes, ref x);
            readFrom2 = CreateIPEndPoint(x.ToString());//todo: this is kinda garbage
            if (i > 0)
            {
                allBytes.AddRange(bytes.Take(i));
            }
            if (allBytes.Count > 0)
            {
                Console.WriteLine($"got {allBytes.Count} UDP bytes from {readFrom2}");
                writeTo.Enqueue(allBytes);
                return true;
            }
            return false;
        }

        public static IPEndPoint CreateIPEndPoint(string endPoint)
        {
            string[] ep = endPoint.Split(':');
            if (ep.Length < 2)
            {
                throw new FormatException("Invalid endpoint format");
            }

            IPAddress ip;
            if (ep.Length > 2)
            {
                if (!IPAddress.TryParse(string.Join(":", ep, 0, ep.Length - 1), out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            else
            {
                if (!IPAddress.TryParse(ep[0], out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            return !int.TryParse(ep[ep.Length - 1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out int port)
                ? throw new FormatException("Invalid port")
                : new IPEndPoint(ip, port);
        }

        private bool Write(Socket writeTo, IPEndPoint writeTo2, Queue<IEnumerable<byte>> readFrom)
        {
            if (readFrom.Count < 1)
            {
                return false;
            }

            while (readFrom.Count > 0)
            {
                int i = 0;
                byte[] bytes = readFrom.Dequeue().ToArray();
                while (i < bytes.Length)
                {

                    i += writeTo.SendTo(bytes, i, bytes.Length - i, SocketFlags.None, writeTo2);
                }
            }
            return true;
        }
    }
}
