﻿using Mono.Nat;

namespace protonportforward
{
    public class RelayConfig
    {
        public RelayConfig(Protocol proto, int listenPort, string listenHost, int targetPort, string targetHost)
        {
            this.proto = proto;
            this.listenPort = listenPort;
            this.listenHost = listenHost;
            this.targetPort = targetPort;
            this.targetHost = targetHost;
        }

        public Protocol proto { get; set; }
        public int listenPort { get; set; }
        public string listenHost { get; set; }
        public int targetPort { get; set; }
        public string targetHost { get; set; }

        public string GetHash()
        {
            return $"{proto}{listenPort}{listenHost}{targetPort}{targetHost}";
        }
    }
}
