﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace protonportforward
{
    internal class PortProxy
    {
        public string ListenAddress { get; set; }
        public int ListenPort { get; set; }
        public string ConnectAddress { get; set; }
        public int ConnectPort { get; set; }

        public NetshProto ListenType { get; set; }
        public NetshProto ConnectType { get; set; }

        public static PortProxy FromNetsh(string line, string header)
        {
            PortProxy pp = new PortProxy();

            Match matches = Regex.Match(header, @"^Listen on ([a-zA-Z0-9]+):\s+Connect to ([a-zA-Z0-9]+):$");
            if (!matches.Success)
            {
                throw new Exception("invalid header while parsing netsh output header");
            }
            pp.ListenType = (NetshProto)Enum.Parse(typeof(NetshProto), matches.Groups[1].Value);
            pp.ConnectType = (NetshProto)Enum.Parse(typeof(NetshProto), matches.Groups[2].Value);


            matches = Regex.Match(line, @"([^\s]+)\s+(\d+)\s+([^\s]+)\s+(\d+)");
            if (!matches.Success)
            {
                throw new Exception("invalid header while parsing netsh output line");
            }
            pp.ListenAddress = IPAddress.Parse(matches.Groups[1].Value).ToString();
            pp.ListenPort = int.Parse(matches.Groups[2].Value);
            pp.ConnectAddress = IPAddress.Parse(matches.Groups[3].Value).ToString();
            pp.ConnectPort = int.Parse(matches.Groups[4].Value);

            return pp;

        }
        public override string ToString()
        {
            return $"{ListenAddress}:{ListenPort} => {ConnectAddress}:{ConnectPort}";
        }
    }
}
