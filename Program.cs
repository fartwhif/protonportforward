﻿using Mono.Nat;

using SharpNetSH;
using SharpNetSH.ADVFIREWALL.FIREWALL.Enums;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace protonportforward
{
    //protonvpn does not respect requested public port
    //the app will be telling the internet about its configured port,
    //so it needs to be reconfigured after this tool sets up the ports
    //i hear there are max of 5, tested and it maxed out at 9 "Error OutOfResources: Could not modify the port map"
    //server couples tcp and udp as long as the requested private port is the same
    //apps that only expect to receive but won't allow configure then change priv port to its hardcoded port and comment out the calls to configure and udp proxy and all you get is the proton forward
    //only works with certain paid servers, tested with IKEv2 auth with username suffixes +pmp+f2+nr
    //if the nat-pmp device is not detected within a few seconds then something is wrong and it won't work

    internal class Program
    {
        private static bool keepRunning = true;
        private static readonly string ruleNamePrefix = "protonportforward";
        private static string interfaceAddress = "";
        private static string gatewayAddress = "";
        private static ManualResetEvent FirstDeviceFound = new ManualResetEvent(false);
        private static NetSH netsh = null;
        private static readonly List<string> configured = new List<string> { };
        private static readonly List<INatDevice> devices = new List<INatDevice>();
        private static readonly ConcurrentDictionary<string, RelayUDP> udpProxies = new ConcurrentDictionary<string, RelayUDP>();
        private static readonly List<string> ports = new List<string>();

        //hardcoded for now, these are private ports, protonvpn only gives random public ports
        private static readonly List<Mapping> desiredMappings = new List<Mapping>() {
            new Mapping(Mono.Nat.Protocol.Tcp, 27364, 0),
            new Mapping(Mono.Nat.Protocol.Udp, 27364, 0),
            new Mapping(Mono.Nat.Protocol.Tcp, 49507, 0),
            new Mapping(Mono.Nat.Protocol.Udp, 49507, 0),
            new Mapping(Mono.Nat.Protocol.Tcp, 22803, 0),
            new Mapping(Mono.Nat.Protocol.Udp, 22803, 0)
        };

        private class MyMapping
        {
            public Mapping mapping { get; set; }
            public bool Proxy { get; set; }
        }

        private static void Cleanup()
        {
            IResponse resp;

            List<string> myRules = new List<string>() {
                $"{ruleNamePrefix}1Tcp",
                $"{ruleNamePrefix}2Udp",
                $"{ruleNamePrefix}3Tcp",
                $"{ruleNamePrefix}4Udp",
                $"{ruleNamePrefix}5Tcp",
                $"{ruleNamePrefix}6Udp"
            };
            foreach (var rule in myRules)
            {
                resp = netsh.AdvFirewall.Firewall.Show.Rule(rule);
                if (resp.IsNormalExit && resp.ExitCode == 0)
                {
                    resp = netsh.AdvFirewall.Firewall.Delete.Rule(rule);
                    Console.WriteLine($"deleted firewall rule {rule}");
                }
            }

            resp = netsh.Interface.PortProxy.Show.all();
            PortProxies pp = PortProxies.FromNetsh(resp);

            foreach (PortProxy p in pp)
            {
                if (p.ListenType == NetshProto.ipv4 && p.ListenAddress.StartsWith(getFirst3Tuples(interfaceAddress)))
                {
                    _ = netsh.Interface.PortProxy.Delete.v4tov4(IPAddress.Parse(p.ListenAddress), p.ListenPort);
                    Console.WriteLine($"deleted proxy {p}");
                }
            }
            foreach (var udpProxy in udpProxies.Values)
            {
                udpProxy.Exiter.Cancel();
                udpProxy.DoneExiting.WaitOne();
            }
            configured.Clear();
            //proton doesn't allow deletion of nat-pmp forwards, you have to connect to a new server (or reconnect?) to clear them.
            Console.WriteLine("cleaup finished");
        }

        private static void Main(string[] args)
        {
            netsh = new NetSH(new CommandLineHarness());

            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                keepRunning = false;
            };
            GetAddresses();
            Cleanup();
            NatUtility.DeviceFound += NatUtility_DeviceFound;
            NatUtility.Search(IPAddress.Parse(gatewayAddress), NatProtocol.Pmp);
            FirstDeviceFound.WaitOne(5000);
            int i = 0;
            Stopwatch sw = Stopwatch.StartNew();
            bool first = true;
            int portsBefore = 0;
            while (true)
            {
                if (!keepRunning)
                {
                    Console.WriteLine();
                    Cleanup();
                    Console.WriteLine();
                    Console.WriteLine("[enter] to quit...");
                    Console.ReadLine();
                    return;
                }
                if (first || sw.Elapsed > TimeSpan.FromSeconds(30))//proton granted nat-pmp port forwards last only 1 minute (not yet confirmed but it responds as if they expire in 60s)
                {
                    first = false;
                    foreach (INatDevice dev in devices)//should just be the 1
                    {
                        i = 0;
                        foreach (Mapping desiredMapping in desiredMappings)
                        {
                            i++;
                            string confName = $"{ruleNamePrefix}{i}{desiredMapping.Protocol}";
                            Mapping granted = dev.CreatePortMap(desiredMapping);
                            Configure(confName, desiredMapping, granted);
                        }
                        if (ports.Count != portsBefore)
                        {
                            portsBefore = ports.Count;
                            Console.WriteLine();
                            Console.WriteLine($"{ports.Count} FORWARDED PORTS:");
                            Console.WriteLine($"--------------");
                            foreach (string port in ports)
                            {
                                Console.WriteLine($"{port}");
                            }
                        }
                    }
                    sw.Restart();
                }
                Thread.Sleep(100);
            }
        }

        private static void Configure(string confName, Mapping desiredMapping, Mapping granted)
        {
            if (configured.Contains(confName))
            {
                return;
            }
            Console.WriteLine(granted.ToString());
            SharpNetSH.ADVFIREWALL.FIREWALL.Enums.Protocol prot = (desiredMapping.Protocol == Mono.Nat.Protocol.Udp) ?
                SharpNetSH.ADVFIREWALL.FIREWALL.Enums.Protocol.Udp : SharpNetSH.ADVFIREWALL.FIREWALL.Enums.Protocol.Tcp;
            SharpNetSH.ADVFIREWALL.FIREWALL.Enums.Action act = SharpNetSH.ADVFIREWALL.FIREWALL.Enums.Action.Allow;
            Direction dir = Direction.In;

            _ = netsh.AdvFirewall.Firewall.Add.Rule(confName, dir, act, protocol: prot, localport: granted.PrivatePort);//add the firewall rule

            Console.WriteLine($"Added firewall rule {confName} - {dir} {act} {prot} {granted.PrivatePort}");

            if (desiredMapping.Protocol == Mono.Nat.Protocol.Tcp)
            {
                IPAddress addr = IPAddress.Parse(interfaceAddress);
                //most apps require private and public port to be the same, but proton service doesn't allow a specific public port
                //so force them the same and then reconfigure the app to use the issued, random public port
                //ideally the app does nat-pmp itself and handles this unusual facet gracefully, such as qBittorent
                //less ideally the app would allow you to tell it 2 ports via RPC or something, one for private and one for public
                //if any of the forwarded ports can't be configured in the app then proxies probably won't work
                _ = netsh.Interface.PortProxy.Add.v4tov4(addr, granted.PrivatePort, addr, granted.PublicPort); //add the port forwarding

                Console.WriteLine($"Added proxy {addr}:{granted.PrivatePort} => {addr}:{granted.PublicPort}");
            }

            if (granted.PublicPort != desiredMapping.PublicPort)//probably don't need this conditional, always true
            {
                switch (granted.Protocol)
                {
                    case Mono.Nat.Protocol.Tcp:
                        //we already set up our windows proxy
                        break;
                    case Mono.Nat.Protocol.Udp:
                        RelayConfig relayConfig = new RelayConfig(granted.Protocol, granted.PrivatePort, interfaceAddress, granted.PublicPort, interfaceAddress);
                        if (!udpProxies.ContainsKey(relayConfig.GetHash()) || (udpProxies.ContainsKey(relayConfig.GetHash()) && udpProxies[relayConfig.GetHash()] == null))
                        {
                            RelayUDP ru = new RelayUDP(relayConfig, udpProxies);
                            Thread t = new Thread(() =>
                            {
                                ru.Run();
                            });
                            t.Start();
                            ru.DoneStarting.WaitOne();
                        }
                        break;
                }
            }
            configured.Add(confName);
            string port = $"{desiredMapping.Protocol}/{interfaceAddress}: {granted.PublicPort}";
            ports.Add(port);
            Console.WriteLine($"Port Forwarded.  Set your application to listen via {port}");
        }

        private static void NatUtility_DeviceFound(object sender, DeviceEventArgs e)
        {
            Console.WriteLine(e.Device.ToString());
            devices.Add(e.Device);
            FirstDeviceFound.Set();
        }
        private static void GetAddresses()
        {
            NetworkInterface[] k = NetworkInterface.GetAllNetworkInterfaces();

            IEnumerable<NetworkInterface> y = NetworkInterface
               .GetAllNetworkInterfaces()
               .Where(n => n.OperationalStatus == OperationalStatus.Up)
               .Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
               .Where(a => ((int)a.NetworkInterfaceType == 53) || a.Name.ToUpper().Contains("OPENVPN") || a.Name.ToUpper().Contains("PROTON"));

            if (y.Count() != 1)
            {
                throw new Exception($"Found either zero or more than one potential network interfaces when searching for ProtonVPN/OpenVPN interface.  Aborting.");
            }

            NetworkInterface _interface = y.First();
            IPInterfaceProperties ipProps = _interface.GetIPProperties();
            interfaceAddress = ipProps.UnicastAddresses.First(b => b.Address.AddressFamily == AddressFamily.InterNetwork).Address.ToString();
            //dns server is the "gateway" that takes nat-pmp requests
            //another strange facet of proton that i think most apps attempting nat-pmp do not expect
            gatewayAddress = ipProps.DnsAddresses.First(b => b.AddressFamily == AddressFamily.InterNetwork).ToString();
        }
        private static string getFirst3Tuples(string ipv4Addr)
        {
            return ipv4Addr.Split('.').Take(3).Aggregate((a, b) => $"{a}.{b}") + ".";
        }
    }
}
