﻿using SharpNetSH;

using System.Collections.Generic;

namespace protonportforward
{
    internal class PortProxies : List<PortProxy>
    {
        public string ListenAddress { get; set; }
        public int ListenPort { get; set; }
        public string ConnectAddress { get; set; }
        public int ConnectPort { get; set; }

        public static PortProxies FromNetsh(IResponse _resp)
        {
            string resp = _resp.Response;
            PortProxies pp = new PortProxies();
            string[] lines = resp.Trim().Replace("\r\n", "\n").Split('\n');

            string header = string.Empty;
            foreach (string _line in lines)
            {
                string line = _line.Trim();
                if (line.StartsWith("Listen"))
                {
                    header = line;
                    continue;
                }
                if (line.StartsWith("Add") || line.StartsWith("---"))
                {
                    continue;
                }
                pp.Add(PortProxy.FromNetsh(line, header));
            }

            return pp;

        }
    }
}
